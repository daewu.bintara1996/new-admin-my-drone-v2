package com.satmei.adminmydrone.base;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.satmei.adminmydrone.utils.MyLog;
import com.satmei.adminmydrone.utils.MyPreferences;
import com.satmei.adminmydrone.utils.MyToast;

public class BaseActivity extends AppCompatActivity {

    public String TOKEN_USER_ID_KEY = "TOKEN_USER_ID_KEY";
    public String TOKEN_EMAIL_KEY = "TOKEN_EMAIL_KEY";
    public String TOKEN_NAME_KEY = "TOKEN_NAME_KEY";
    public String TOKEN_IMAGE_KEY = "TOKEN_IMAGE_KEY";

    public MyPreferences myPreferences;
    public MyToast myToast;
    public MyLog myLog = new MyLog();

    public FirebaseDatabase database;
    public DatabaseReference myRef;

    public BaseActivity() {
        myPreferences = new MyPreferences(this);
        myToast = new MyToast(this);
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }
}
