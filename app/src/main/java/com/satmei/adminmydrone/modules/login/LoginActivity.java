package com.satmei.adminmydrone.modules.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.satmei.adminmydrone.R;
import com.satmei.adminmydrone.base.BaseActivity;
import com.satmei.adminmydrone.modules.dashboard.DashboardActivity;

public class LoginActivity extends BaseActivity {
    GoogleSignInOptions gso;
    private FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1;
    private int RC_SIGN_IN_CANCELED = 12501;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
              .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut();
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                myLog.d( "GOOGLE MAIL : " + account.getEmail());
                myLog.d( "NAME : " + account.getDisplayName());
                myLog.d( "IMAGE : " + account.getPhotoUrl());

                myPreferences.savePreferences(TOKEN_USER_ID_KEY, account.getId());
                myPreferences.savePreferences(TOKEN_EMAIL_KEY, account.getEmail());
                myPreferences.savePreferences(TOKEN_NAME_KEY, account.getDisplayName());
                myPreferences.savePreferences(TOKEN_IMAGE_KEY, account.getPhotoUrl() + "");
                myToast.showLongToast("Berhasil Login");

                myRef = database.getReference("users");
                myRef.child(account.getId()).child("email").setValue(account.getEmail());

                Intent intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                finish();

            } catch (ApiException e) {
                myLog.d("FAILED CODE : "+String.valueOf(e.getStatusCode()));
                if (e.getStatusCode() == RC_SIGN_IN_CANCELED) {
                    myToast.showLongToast("Membatalkan autentikasi");
                } else {
                    myToast.showLongToast("Uppsss... Terjadi kesalahan coba beberapa saat lagi");
                }
            }
        }
    }

    public void btnLogin(View view) {
        mAuth.signOut();
        mGoogleSignInClient.signOut();
        signIn();
    }
}