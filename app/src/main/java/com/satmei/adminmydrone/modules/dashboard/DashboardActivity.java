package com.satmei.adminmydrone.modules.dashboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.satmei.adminmydrone.R;
import com.satmei.adminmydrone.adapter.SensorAdapter;
import com.satmei.adminmydrone.base.BaseActivity;
import com.satmei.adminmydrone.models.FirebaseDataSensor;
import com.satmei.adminmydrone.modules.login.LoginActivity;
import com.satmei.adminmydrone.modules.splash.SplashActivity;

import java.util.ArrayList;
import java.util.Collections;

public class DashboardActivity extends BaseActivity{

    //    TextView tvAx, tvAy, tvAz, tvGx, tvGy, tvGz, tvMFx, tvMFy, tvMFz, tvEmail;
    TextView tvEmail;
    ListView rvDataSensor;
    ArrayList<FirebaseDataSensor> dataSensors = new ArrayList<>();
    SensorAdapter sensorAdapter;
    Handler handler = new Handler();
    Double tinggi;
    Double a = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        sensorAdapter = new SensorAdapter(dataSensors, this);
//        tvAx = findViewById(R.id.tvAx);
//        tvAy = findViewById(R.id.tvAy);
//        tvAz = findViewById(R.id.tvAz);
//        tvGx = findViewById(R.id.tvGx);
//        tvGy = findViewById(R.id.tvGy);
//        tvGz = findViewById(R.id.tvGz);
//        tvMFx = findViewById(R.id.tvMFx);
//        tvMFy = findViewById(R.id.tvMFy);
//        tvMFz = findViewById(R.id.tvMFz);
        tvEmail = findViewById(R.id.tvEmail);
        rvDataSensor = findViewById(R.id.rvDataSensor);

//        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        rvDataSensor.setLayoutManager(linearLayoutManager);
        rvDataSensor.setAdapter(sensorAdapter);

//        tvAx.setText("X : ");
//        tvAy.setText("Y : ");
//        tvAz.setText("Z : ");
//        tvGx.setText("X : ");
//        tvGy.setText("Y : ");
//        tvGz.setText("Z : ");
//        tvMFx.setText("X : ");
//        tvMFy.setText("Y : ");
//        tvMFz.setText("Z : ");

        tvEmail.setText(myPreferences.getPreferencesString(TOKEN_EMAIL_KEY));

        // onRecivedSensorDatas();



    }

    private void onRecivedSensorDatas() {
        String USER_ID = myPreferences.getPreferencesString(TOKEN_USER_ID_KEY);

        if (USER_ID == null){
            myPreferences.clearAllPreferences();
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        myRef = database.getReference("user-sensors");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                a = a+2;
                Log.d("pangkat",   a.toString() );

                myLog.d("ALALAKALAKALAK DATA 1 : " + dataSnapshot.child(USER_ID).child("accelerometer").child("X").getValue().toString());
                try {
                    FirebaseDataSensor fbDataSensor = new FirebaseDataSensor();

                    Double accz = Double.parseDouble(dataSnapshot.child(USER_ID).child("accelerometer").child("Z").getValue().toString())  ;
                    tinggi = Math.pow(2 ,a);
                    Double x = 0.5* (accz-9.8) * 2  ;
                    Double res = 2 + x ;
                    Log.d("tinggi", "onDataChange: "+ x);

                    fbDataSensor.setTinggi(res.toString());

                    fbDataSensor.setsAx(dataSnapshot.child(USER_ID).child("accelerometer").child("X").getValue().toString());
                    fbDataSensor.setsAy(dataSnapshot.child(USER_ID).child("accelerometer").child("Y").getValue().toString());
                    fbDataSensor.setsAz(accz.toString());


                    fbDataSensor.setsGx(dataSnapshot.child(USER_ID).child("gyroscope").child("X").getValue().toString());
                    fbDataSensor.setsGy(dataSnapshot.child(USER_ID).child("gyroscope").child("Y").getValue().toString());
                    fbDataSensor.setsGz(dataSnapshot.child(USER_ID).child("gyroscope").child("Z").getValue().toString());

                    fbDataSensor.setsMFx(dataSnapshot.child(USER_ID).child("magneticField").child("X").getValue().toString());
                    fbDataSensor.setsMFy(dataSnapshot.child(USER_ID).child("magneticField").child("Y").getValue().toString());
                    fbDataSensor.setsMFz(dataSnapshot.child(USER_ID).child("magneticField").child("Z").getValue().toString());

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            myLog.d("ALALAKALAKALAK DATA 2 : " + dataSnapshot.child(USER_ID).child("accelerometer").child("X").getValue().toString());
                            dataSensors.add(fbDataSensor);
                            myLog.d("ALALAKALAKALAK DATA 3 : "+sensorAdapter.getItem(sensorAdapter.getCount() - 1).getsAx());
                            try {
                                //sensorAdapter.addAll(dataSensors);
                                sensorAdapter.notifyDataSetChanged();
                                rvDataSensor.setSelection(sensorAdapter.getCount() - 1);
                                handler.removeCallbacksAndMessages(null);
                            } catch (Exception e) {
                                myToast.showShortToast("Memory limit :(");
                            }
                        }
                    }, 2000);
                } catch (Exception e){
                    myRef.removeEventListener(this);
                    alertDataNotFound();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void alertDataNotFound() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Data tidak ada");
        builder.setMessage("Pastikan Aplikasi MyDrone login dengan akun yang sama");
        builder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                onRecivedSensorDatas();
            }
        });
        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                finish();

            }
        });
        builder.setNeutralButton("Logout", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                onLogounClick();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void onLogounClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Keluar Akun");
        builder.setMessage("Yakin akan keluar dari akun ini?");
        builder.setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                myPreferences.clearAllPreferences();
                myPreferences.clearAllPreferences();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void btnLogout(View view) {
        onLogounClick();
    }


    public void btnClear(View view) {
        if (sensorAdapter.getCount() != 0) {
            sensorAdapter.clear();
        }
    }


    public void btnStart(View view) {
        // DIDALAM FUNGSI TERDAPAT LOGIC NYA
        // goOnline() fungsi milik google
        database.goOnline();
        onRecivedSensorDatas();
    }

    public void btnStop(View view) {
        // BUAT LOGIC BUAT STOP SUBSCRIBE DATA DARI FIREBASE
        myRef = database.getReference("user-sensors");
        // goOffline() fungsi milik google
        database.goOffline();
    }


}

