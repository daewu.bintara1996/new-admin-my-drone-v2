package com.satmei.adminmydrone.models;

public class FirebaseDataSensor {
    private String sAx;
    private String sAy;
    private String sAz;

    private String sGx;
    private String sGy;
    private String sGz;

    private String sMFx;
    private String sMFy;
    private String sMFz;

    private String tinggi;

    public FirebaseDataSensor() {
    }

    public String getsAx() {
        return sAx;
    }

    public void setsAx(String sAx) {
        this.sAx = sAx;
    }

    public String getsAy() {
        return sAy;
    }

    public void setsAy(String sAy) {
        this.sAy = sAy;
    }

    public String getsAz() {
        return sAz;
    }

    public void setsAz(String sAz) {
        this.sAz = sAz;
    }

    public String getsGx() {
        return sGx;
    }

    public void setsGx(String sGx) {
        this.sGx = sGx;
    }

    public String getsGy() {
        return sGy;
    }

    public void setsGy(String sGy) {
        this.sGy = sGy;
    }

    public String getsGz() {
        return sGz;
    }

    public void setsGz(String sGz) {
        this.sGz = sGz;
    }

    public String getsMFx() {
        return sMFx;
    }

    public void setsMFx(String sMFx) {
        this.sMFx = sMFx;
    }

    public String getsMFy() {
        return sMFy;
    }

    public void setsMFy(String sMFy) {
        this.sMFy = sMFy;
    }

    public String getsMFz() {
        return sMFz;
    }

    public void setsMFz(String sMFz) {
        this.sMFz = sMFz;
    }

    public String getTinggi() {
        return tinggi;
    }

    public void setTinggi(String tinggi) {
        this.tinggi = tinggi;
    }
}
