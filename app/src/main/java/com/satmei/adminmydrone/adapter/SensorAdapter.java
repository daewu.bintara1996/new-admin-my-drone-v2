package com.satmei.adminmydrone.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.satmei.adminmydrone.R;
import com.satmei.adminmydrone.models.FirebaseDataSensor;

import java.util.ArrayList;

public class SensorAdapter extends ArrayAdapter<FirebaseDataSensor> implements View.OnClickListener{

    private ArrayList<FirebaseDataSensor> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView tvAx, tvAy, tvAz, tvGx, tvGy, tvGz, tvMFx, tvMFy, tvMFz,tinggi;
    }

    public SensorAdapter(ArrayList<FirebaseDataSensor> data, Context context) {
        super(context, R.layout.item_data_sensors, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        FirebaseDataSensor dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;
        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_data_sensors, parent, false);
            viewHolder.tvAx = convertView.findViewById(R.id.tvAx);
            viewHolder.tvAy = convertView.findViewById(R.id.tvAy);
            viewHolder.tvAz = convertView.findViewById(R.id.tvAz);
            viewHolder.tvGx = convertView.findViewById(R.id.tvGx);
            viewHolder.tvGy = convertView.findViewById(R.id.tvGy);
            viewHolder.tvGz = convertView.findViewById(R.id.tvGz);
            viewHolder.tvMFx = convertView.findViewById(R.id.tvMFx);
            viewHolder.tvMFy = convertView.findViewById(R.id.tvMFy);
            viewHolder.tvMFz = convertView.findViewById(R.id.tvMFz);
            viewHolder.tinggi = convertView.findViewById(R.id.tinggi);

            if (dataModel == null){
                return convertView;
            }
            if (dataModel.getsAx() == null){
                return convertView;
            }
            if (dataModel.getsAx().isEmpty()){
                return convertView;
            }

            Log.d("ADAPTER","DATA"+dataModel.getsAx().toString());

            viewHolder.tvAx.setText("X : "+dataModel.getsAx().toString());
            viewHolder.tvAy.setText("Y : "+dataModel.getsAy().toString());
            viewHolder.tvAz.setText("Z : "+dataModel.getsAz().toString());

            viewHolder.tinggi.setText("Tinggi :"+dataModel.getTinggi().toString());

            viewHolder.tvGx.setText("X : "+dataModel.getsGx().toString());
            viewHolder.tvGy.setText("Y : "+dataModel.getsGy().toString());
            viewHolder.tvGz.setText("Z : "+dataModel.getsGz().toString());

            viewHolder.tvMFx.setText("X : "+dataModel.getsMFx().toString());
            viewHolder.tvMFy.setText("Y : "+dataModel.getsMFy().toString());
            viewHolder.tvMFz.setText("Z : "+dataModel.getsMFz().toString());

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_data_sensors, parent, false);
            viewHolder.tvAx = convertView.findViewById(R.id.tvAx);
            viewHolder.tvAy = convertView.findViewById(R.id.tvAy);
            viewHolder.tvAz = convertView.findViewById(R.id.tvAz);
            viewHolder.tvGx = convertView.findViewById(R.id.tvGx);
            viewHolder.tvGy = convertView.findViewById(R.id.tvGy);
            viewHolder.tvGz = convertView.findViewById(R.id.tvGz);
            viewHolder.tvMFx = convertView.findViewById(R.id.tvMFx);
            viewHolder.tvMFy = convertView.findViewById(R.id.tvMFy);
            viewHolder.tvMFz = convertView.findViewById(R.id.tvMFz);
            viewHolder.tinggi = convertView.findViewById(R.id.tinggi);

            if (dataModel == null){
                return convertView;
            }
            if (dataModel.getsAx() == null){
                return convertView;
            }
            if (dataModel.getsAx().isEmpty()){
                return convertView;
            }

            Log.d("ADAPTER","DATA"+dataModel.getsAx().toString());

            viewHolder.tvAx.setText("X : "+dataModel.getsAx().toString());
            viewHolder.tvAy.setText("Y : "+dataModel.getsAy().toString());
            viewHolder.tvAz.setText("Z : "+dataModel.getsAz().toString());

            viewHolder.tvGx.setText("X : "+dataModel.getsGx().toString());
            viewHolder.tvGy.setText("Y : "+dataModel.getsGy().toString());
            viewHolder.tvGz.setText("Z : "+dataModel.getsGz().toString());

            viewHolder.tvMFx.setText("X : "+dataModel.getsMFx().toString());
            viewHolder.tvMFy.setText("Y : "+dataModel.getsMFy().toString());
            viewHolder.tvMFz.setText("Z : "+dataModel.getsMFz().toString());

            viewHolder.tinggi.setText("Tinggi :"+dataModel.getTinggi().toString());

            result=convertView;

            convertView.setTag(viewHolder);
        }

        lastPosition = position;
        // Return the completed view to render on screen
        return convertView;
    }
}
